#!/usr/bin/env python

import json
import sys

def main():
  fn = sys.argv[1]
  lines = open(fn).readlines()
  chars = []
  for i in range(128):
    chars.append({"w":0,"h":0})
  base = None
  for l in lines:
    l = l.strip()
    if l.startswith("common"):
      d = dict([kv.split("=") for kv in l.split()][1:])
      base = int(d["base"])
      print base

    if l.startswith("char "):
      d = dict([(k,int(v)) for (k,v) in [kv.split("=") for kv in l.split()][1:]])
      id,x,y,w,h = d["id"],d["x"],d["y"],d["width"],d["height"]
      xadv,xoff,yoff = d["xadvance"],d["xoffset"],d["yoffset"]
      yoff = base - h - yoff
      chars[id] = {"w":w,"h":h,"x":x,"y":y,"xoff":xoff,"yoff":yoff,"xadv":xadv}
  json.dump(chars, open(fn.replace("fnt","json"), "wb"))

main()
