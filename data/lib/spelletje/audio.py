import cspelletje

class Sound(object):
  def __init__(self, fn):
    self.id = cspelletje.sound_load(fn)

  def play(self):
    cspelletje.sound_play(self.id)


class Music(object):
  def __init__(self, fn):
    self.id = cspelletje.music_load(fn)

  def play(self):
    cspelletje.music_play(self.id)


class Audio(object):
  def __init__(self):
    self._sounds = {}

  def loadSound(self, fn):
    if fn not in self._sounds:
      self._sounds[fn] = Sound(fn)
    return self._sounds[fn]

  def loadMusic(self, fn):
    if fn not in self._sounds:
      self._sounds[fn] = Music(fn)
    return self._sounds[fn]
