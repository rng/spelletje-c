#ifndef _OGL_H_
#define _OGL_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void ogl_init(int w, int h, int scalefactor);
void ogl_start_frame();
void ogl_finish_frame();
void ogl_set_bg_color(float r, float g, float b);
uint32_t ogl_load_texture(int w, int h, uint8_t *data);
void ogl_draw_quads(int texid, int n, float* pos, float* tex, uint8_t* col);

#ifdef __cplusplus
}
#endif

#endif
