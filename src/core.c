#include <Python.h>
#include <stdint.h>
#include "core.h"
#include "image.h"
// FIXME: the cspelletje.h include doesn't work on windows
//#include "cspelletje.h"
enum CallbackIds {
  CB_DRAW = 1,
  CB_UPDATE = 2,
  CB_RESIZE = 3,
  CB_KEYDOWN = 4,
  CB_KEYUP = 5,
  CB_MOUSEPRESSED = 6,
  CB_MOUSERELEASED = 7,
  CB_MOUSEMOVED = 8,
};

void callback(int);
void callback_i(int, int);
void callback_f(int, float);
void callback_ii(int, int, int);
void callback_iii(int, int, int, int);
int initcspelletje(void);


static const char * PYBOOT = "\
import sys\n\
sys.path = [ './data','./data/lib','python27.zip', ]\
\n";

static Backend *backend;


void _checkpy(int v) {
  if (v<0) {
    printf("Exception occurred\n");
    exit(-1);
  }
}


void init(Backend *b) {
  backend = b;
  Py_NoSiteFlag=1;
  Py_SetProgramName("spelletje");
  Py_SetPythonHome(".");
  Py_InitializeEx(0);
  PyRun_SimpleString(PYBOOT);
  initcspelletje();
  int r = PyRun_SimpleString("from script import main\n");
  _checkpy(r);
}


void shutdown() {
  Py_Finalize();
}


void draw() {
  callback(CB_DRAW);
}

void update(float dt) {
  callback_f(CB_UPDATE, dt);
}

void keyup(int sym, int mod) {
  callback_ii(CB_KEYUP, sym, mod);
}

void keydown(int sym, int mod) {
  callback_ii(CB_KEYDOWN, sym, mod);
}

void mousepressed(int x, int y, int button) {
  callback_iii(CB_MOUSEPRESSED, x, y, button);
}

void mousereleased(int x, int y, int button) {
  callback_iii(CB_MOUSERELEASED, x, y, button);
}

void mousemoved(int x, int y) {
  callback_ii(CB_MOUSEMOVED, x, y);
}

void resize(int w, int h, int scalefactor) {
  callback_iii(CB_RESIZE, w, h, scalefactor);
}


void core_create(Core *core) {
  core->init = init;
  core->shutdown = shutdown;
  core->draw = draw;
  core->update = update;
  core->keyup = keyup;
  core->keydown = keydown;
  core->mousepressed = mousepressed;
  core->mousereleased = mousereleased;
  core->mousemoved = mousemoved;
  core->resize = resize;
}


void core_set_title(char *title) {
  backend->set_title(backend, title);
}


void core_set_resolution(int w, int h) {
  backend->set_resolution(backend, w, h);
}


void core_set_scaling(int factor, int mode) {
  backend->set_scaling(backend, factor, mode);
}


void core_set_bg_color(float r, float g, float b) {
  backend->set_bg_color(backend, r,g,b);
}


typedef struct __attribute__ ((__packed__)) {
  float x, y, cx, cy;
  float w, h;
  float tx0,ty0,tx1,ty1;
  float angle;
  float r,g,b,a;
  float pad;
} Sprite;


void core_draw_sprites(int spriteid, int n, float *sprites) {
  Sprite *spr = (Sprite*)sprites;
  float* pos = (float*)malloc(n*8*sizeof(float));
  float* tex = (float*)malloc(n*8*sizeof(float));
  uint8_t* col = (uint8_t*)malloc(n*16*sizeof(uint8_t));
  for (int i=0;i<n;i++) {
    float x = spr[i].x;
    float y = spr[i].y;
    float cx = spr[i].cx;
    float cy = spr[i].cy;
    int w = (int)(spr[i].w);
    int h = (int)(spr[i].h);
    float angle = spr[i].angle;

    float x0=-cx, y0=-cy, x1=w-cx, y1=-cy, x2=w-cx, y2=h-cy, x3=-cx, y3=h-cy;
    float ca = cos(angle);
    float sa = sin(angle);
#define TX(x,y) (x*ca-y*sa)
#define TY(x,y) (x*sa+y*ca)
    pos[i*8+0] = x+TX(x0,y0);
    pos[i*8+1] = y+TY(x0,y0);
    pos[i*8+2] = x+TX(x1,y1);
    pos[i*8+3] = y+TY(x1,y1);
    pos[i*8+4] = x+TX(x2,y2);
    pos[i*8+5] = y+TY(x2,y2);
    pos[i*8+6] = x+TX(x3,y3);
    pos[i*8+7] = y+TY(x3,y3);

    tex[i*8+0] = spr[i].tx0;
    tex[i*8+1] = spr[i].ty0;
    tex[i*8+2] = spr[i].tx1;
    tex[i*8+3] = spr[i].ty0;
    tex[i*8+4] = spr[i].tx1;
    tex[i*8+5] = spr[i].ty1;
    tex[i*8+6] = spr[i].tx0;
    tex[i*8+7] = spr[i].ty1;

    for (int j=0;j<4;j++) {
      col[i*16+j*4+0] = spr[i].r*255;
      col[i*16+j*4+1] = spr[i].g*255;
      col[i*16+j*4+2] = spr[i].b*255;
      col[i*16+j*4+3] = spr[i].a*255;
    }
  }
  backend->draw_quads(backend, spriteid, n*4, pos, tex, col);
  free(pos);
  free(tex);
  free(col);
}

int core_load_sprite(const char* fn, int* w, int* h) {
  uint8_t* data = image_load_png(fn, w, h);
  return backend->create_image(backend, *w, *h, data);
}

int core_sound_load(const char *fn) {
  return backend->sound_load(backend, fn);
}

void core_sound_play(int n) {
  backend->sound_play(backend, n);
}

int core_music_load(const char *fn) {
  return backend->music_load(backend, fn);
}

void core_music_play(int n) {
  backend->music_play(backend, n);
}

void core_quit() {
  backend->quit(backend);
}
