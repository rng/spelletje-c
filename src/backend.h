#ifndef _BACKEND_H_
#define _BACKEND_H_

#include <stdint.h>

typedef struct Backend {
  void (*set_title)(struct Backend*, const char*);
  void (*set_fps)(struct Backend*, int);
  void (*set_resolution)(struct Backend*, int, int);
  void (*set_scaling)(struct Backend*, int, int);
  void (*set_bg_color)(struct Backend*, float, float, float);
  int (*create_image)(struct Backend*, int, int, uint8_t*);
  void (*draw_quads)(struct Backend*, int, int, float*, float*, uint8_t*);
  int (*sound_load)(struct Backend*, const char *);
  void (*sound_play)(struct Backend*, int);
  int (*music_load)(struct Backend*, const char *);
  void (*music_play)(struct Backend*, int);
  void (*quit)(struct Backend*);
  void *priv;
} Backend;

#endif
