#ifndef _CORE_H_
#define _CORE_H_ 

#include "backend.h"

typedef struct Core {
  void (*init)(Backend* backend);
  void (*shutdown)();
  void (*update)(float dt);
  void (*draw)();
  void (*keyup)(int sym, int mod);
  void (*keydown)(int sym, int mod);
  void (*mousepressed)(int x, int y, int button);
  void (*mousereleased)(int x, int y, int button);
  void (*mousemoved)(int x, int y);
  void (*resize)(int w, int h, int scalefactor);
} Core;

void core_create(Core *core);

void core_set_title(char *title);
void core_set_bg_color(float r, float g, float b);
void core_set_resolution(int w, int h);
void core_set_scaling(int factor, int mode);
void core_draw_sprites(int spriteid, int n, float *sprites);
int core_load_sprite(const char* fn, int* w, int* h);
int core_sound_load(const char *fn);
void core_sound_play(int n);
int core_music_load(const char *fn);
void core_music_play(int n);
void core_quit();

#endif
