import cspelletje
import json


class Sprite(object):
  def __init__(self, render, fn, fw, fh):
    self.render = render
    self.id, self.w, self.h = cspelletje.load_sprite(fn)
    self.fw = self.w if fw == 0 else fw
    self.fh = self.h if fw == 0 else fh
    self.framestride = self.w/self.fw

  def draw(self, x, y, frame=0, cx=0, cy=0, scale=1, angle=0, layer=0, w=0,h=0, flip=False, col=(1,1,1,1)):
    fw, fh = self.fw/float(self.w), self.fh/float(self.h)
    if w == 0: 
      w = self.fw
    if h == 0: 
      h = self.fh
    w = float(w)*scale
    h = float(h)*scale

    fx = frame%self.framestride
    fy = frame/self.framestride
    tx0,tx1 = fw*fx, fw*(fx+1)
    ty0,ty1 = fh*fy, fh*(fy+1)
    if flip:
      tx0, tx1 = tx1,tx0

    cx *= scale
    cy *= scale
    self.render.drawSprite(self.id, layer, x, y, cx, cy, w, h, 
                           tx0,ty0,tx1,ty1, angle, col)
  
  def drawCentred(self, x, y, frame=0, scale=1, angle=0, layer=0, w=0, h=0, col=(1,1,1,1)):
    w = w if w else self.fw
    h = h if h else self.fh
    self.draw(x,y, frame, w/2., h/2., scale, angle, layer, w, h, col)


class SpriteAtlas(object):
  def __init__(self, render, fn):
    self.render = render
    self.id, self.w, self.h = cspelletje.load_sprite(fn)
    self.atlas = json.load(open(fn.replace("png","json")))

  def names(self):
    return self.atlas.keys()

  def named(self, name):
    return SpriteAtlasSprite(self.id, self.render, self.w, self.h,
                             name, self.atlas[name])


class SpriteAtlasSprite(object):
  def __init__(self, id, render, aw, ah, name, props):
    self.id = id
    self.render, self.name = render, name
    self.aw, self.ah = aw, ah
    self.props = props
    self.w, self.h = props[0]["size"]
    self.frames = len(props)

  def draw(self, x, y, frame=0, cx=0, cy=0, scale=1, angle=0, layer=0, w=0,h=0, col=(1,1,1,1)):
    if w == 0: 
      w = self.w
    if h == 0: 
      h = self.h
    w = float(w)*scale
    h = float(h)*scale

    fx,fy = self.props[frame]["xy"]
    fw,fh = self.props[frame]["size"]
    tx0, tx1 = (fx)/float(self.aw), (fx+fw)/float(self.aw)
    ty0, ty1 = (fy)/float(self.ah), (fy+fh)/float(self.ah)

    cx *= scale
    cy *= scale
    self.render.drawSprite(self.id, layer, x, y, cx, cy, w, h, 
                           tx0,ty0,tx1,ty1, angle, col)
  
  def drawCentred(self, x, y, frame=0, scale=1, angle=0, layer=0, w=0, h=0, col=(1,1,1,1)):
    w = w if w else self.w
    h = h if h else self.h
    self.draw(x,y, frame, w/2., h/2., scale, angle, layer, w, h, col)
