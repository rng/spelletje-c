#include <stdint.h>
#include <GL/gl.h>

//GLuint fb = 0;

void ogl_init(int w, int h, int scalefactor) {
  //printf("resize! %d,%d:%d\n", w, h, scalefactor);
  int vw = w/scalefactor;
  int vh = h/scalefactor;
  //printf("view: %d,%d\n", vw, vh);
  glMatrixMode (GL_PROJECTION);
  glLoadIdentity ();
  glOrtho (0, vw, vh, 0, 0, 1);
  glMatrixMode (GL_MODELVIEW);
  glEnable(GL_BLEND);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  // setup bg framebuffer
  //if (fb != 0) {
  //  glDeleteFramebuffers(1, &fb);
  //}
  //glGenFramebuffers(1, &fb);
  //printf("fb: %d\n", fb);
}


void ogl_set_bg_color(float r, float g, float b) {
  glClearColor(r,g,b,1);
}


void ogl_start_frame() {
  //glBindFramebuffer(GL_FRAMEBUFFER, fb);
  glClear(GL_COLOR_BUFFER_BIT);
}


void ogl_finish_frame() {
  //glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


uint32_t ogl_load_texture(int w, int h, uint8_t *data) {
  GLuint tex;
  glEnable(GL_TEXTURE_2D);
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, // target
	       0,  // level
	       GL_RGBA, // internalformat
	       w, h,
	       0,  // border
	       GL_RGBA,  // format
	       GL_UNSIGNED_BYTE, // type
         data);
  return tex;
}

void ogl_draw_quads(int texid, int n, float* pos, float* tex, uint8_t* col) {
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, texid);
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  // draw quads
  glVertexPointer(2, GL_FLOAT, 0, pos);
  glTexCoordPointer(2, GL_FLOAT, 0, tex);
  glColorPointer(4, GL_UNSIGNED_BYTE, 0, col);
  glDrawArrays(GL_QUADS, 0, n);
  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);
}

