#ifndef _BACKEND_SFML_H_
#define _BACKEND_SFML_H_

#include "core.h"

#ifdef __cplusplus
extern "C" {
#endif

int sfml_main(Core *core);

#ifdef __cplusplus
}
#endif

#endif
