
class Mouse(object):
  LEFT = 0
  MIDDLE = 2
  RIGHT = 1

  def __init__(self):
    self._pressed = {}

  def mouse_press(self, button):
    self._pressed[button] = True

  def mouse_release(self, button):
    self._pressed[button] = False

  def pressed(self, button):
    return self._pressed.get(button, False)
