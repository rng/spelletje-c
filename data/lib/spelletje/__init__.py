import cspelletje
import engine as _engine
import render as _render
import keys as _keys
import mouse as _mouse
import audio as _audio

engine = _engine.Engine()

render = _render.Render()
keys = _keys.Keys()
mouse = _mouse.Mouse()
audio = _audio.Audio()

def cb(fn):
  def f(*args):
    try:
      return fn(*args)
    except:
      import traceback
      traceback.print_exc()
      print
      cspelletje.quit()
  return f

def run(startstate, states):
  engine.render = render
  engine.keys = keys
  engine.mouse = mouse
  engine.audio = audio
  engine.start(states, startstate)
  cspelletje.register(cspelletje.CB_DRAW, cb(engine.draw))
  cspelletje.register(cspelletje.CB_UPDATE, cb(engine.update))
  cspelletje.register(cspelletje.CB_RESIZE, cb(engine.resize))
  cspelletje.register(cspelletje.CB_KEYDOWN, cb(engine.key_press))
  cspelletje.register(cspelletje.CB_KEYUP, cb(engine.key_release))
  cspelletje.register(cspelletje.CB_MOUSEPRESSED, cb(engine.mouse_press))
  cspelletje.register(cspelletje.CB_MOUSERELEASED, cb(engine.mouse_release))
  cspelletje.register(cspelletje.CB_MOUSEMOVED, cb(engine.mouse_move))
