
class Keys(object):
  X = 23
  Y = 24
  Z = 25
  NUM0 = 26
  NUM1 = 27
  NUM2 = 28
  NUM3 = 29
  NUM4 = 30
  NUM5 = 31
  NUM6 = 32
  NUM7 = 33
  NUM8 = 34
  NUM9 = 35
  ESCAPE = 36
  SPACE = 57
  RETURN = 58
  LEFT = 71
  RIGHT = 72
  UP = 73
  DOWN = 74

  def __init__(self):
    self._pressed = {}

  def key_press(self, sym, mods):
    self._pressed[sym] = True

  def key_release(self, sym, mods):
    self._pressed[sym] = False

  def pressed(self, sym):
    return self._pressed.get(sym, False)
