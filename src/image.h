#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <stdint.h>

uint8_t* image_load_png(const char* filename, int* w, int* h);

#endif
