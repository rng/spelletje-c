# vim:set ts=2 sw=2 sts=2 et:

class Collision(object):
  def __init__(self, px, py):
    self.collx = False
    self.colly = False
    self.px = px
    self.py = py
    self.tx = self.ty = -1


class Collidable(object):
  def collide(self, other, dt):
    raise NotImplementedError()
