# vim:set ts=2 sw=2 sts=2 et:
import json
from .tilemap import CollisionLayer, ObjectLayer, TileLayer
#import os

def loadJSON(map, fn):
  map.clear()
  js = json.load(open(fn+".json"))
  # load tileset
  tilesets = js["tilesets"]
  tsfn = fn+".png"#os.path.join(os.path.dirname(fn), tilesets[0]["image"])
  tsw = tilesets[0]["tilewidth"]
  tsh = tilesets[0]["tileheight"]
  ts = map.engine.render.loadSprite(tsfn, tsw, tsh)
  # set size
  map.setSize(js["width"], js["height"])
  # load layers
  layernum = 0
  for i, jslayer in enumerate(js["layers"]):
    if jslayer["name"] == "collision":
      T = CollisionLayer
    elif "objects" in jslayer:
      T = ObjectLayer
    else:
      T = TileLayer
      layernum += 1
    layer = T(map, jslayer["name"], jslayer["width"], jslayer["height"], 
              tsw, tsh, layernum)
    if isinstance(layer, TileLayer):
      layer.setTileset(ts)
      layer.setOpacity(jslayer["opacity"])
    if isinstance(layer, ObjectLayer):
      for obj in jslayer["objects"]:
        layer.addObject(obj)
    else:
      for x in range(map.width):
        for y in range(map.height):
          layer.setTile(x, y, jslayer["data"][y*map.width+x])
    map.addLayer(layer)

