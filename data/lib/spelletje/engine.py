import cspelletje
from .state import Scene
from .animation import Transition


class Engine(object):
  def __init__(self):
    #self.save = Saver()
    pass
  
  def setTitle(self, title):
    cspelletje.set_title(title)

  def setResolution(self, w, h):
    cspelletje.set_resolution(w, h)

  def setScaling(self, factor, mode=0):
    cspelletje.set_scaling(factor, mode)

  def start(self, states, startstate):
    self.states = dict([(name,state()) for (name,state) in states.items()])
    self.startstate = startstate
    self.states["null"] = Scene()
    self.state = "null"
    self.switchScene(self.startstate, 0.6, True)

  def switchScene(self, newstate, t=0.4, first=False, args=[]):
    if t == 0:
      self.enterScene(newstate, args)
    else:
      self.transition = Transition(self, newstate, t, first=first, args=args)

  def enterScene(self, newstate, args):
    self.states[self.state].leaveScene()
    self.state = newstate
    self.states[self.state].enterScene(args)

  def resize(self, w, h, scalefactor):
    self.render.resize(w, h, scalefactor)

  def draw(self):
    self.render.startFrame(self.states[self.state])
    self.states[self.state].draw()
    if self.transition:
      self.transition.draw()
    self.render.endFrame()
    #self.states[self.state].postDraw()

  def update(self, dt):
    if self.transition:
      if self.transition.update(dt):
        self.transition = None
    self.states[self.state].update(dt)

  def quit(self):
    cspelletje.quit()

  def key_press(self, sym, mods):
    self.keys.key_press(sym, mods)
    if self.transition: return
    self.states[self.state].key_press(sym, mods)

  def key_release(self, sym, mods):
    self.keys.key_release(sym, mods)
    if self.transition: return
    self.states[self.state].key_release(sym, mods)

  def mouse_press(self, x, y, button):
    self.mouse.mouse_press(button)
    if self.transition: return
    self.states[self.state].mouse_press(x, y, button)
  
  def mouse_release(self, x, y, button):
    self.mouse.mouse_release(button)
    if self.transition: return
    self.states[self.state].mouse_release(x, y, button)
  
  def mouse_move(self, x, y):
    if self.transition: return
    self.states[self.state].mouse_move(x, y)
