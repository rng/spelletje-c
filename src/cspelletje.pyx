from libc.stdlib cimport malloc, free

cdef extern from "core.h":
  ctypedef unsigned char uint8_t
  void core_set_title(char *title)
  void core_set_resolution(int w, int h)
  void core_set_scaling(int factor, int mode)
  void core_set_bg_color(float r, float g, float b)
  void core_draw_quads(int sprite, int n, int* pos, float* tex, uint8_t* col)
  void core_draw_sprites(int spriteid, int n, float *sprites)
  int core_load_sprite(const char *fn, int* w, int* h)
  int core_sound_load(const char *fn)
  void core_sound_play(int n)
  int core_music_load(const char *fn)
  void core_music_play(int n)
  void core_quit()

cdef public enum CallbackIds:
  CB_DRAW = 1
  CB_UPDATE = 2
  CB_RESIZE = 3
  CB_KEYDOWN = 4
  CB_KEYUP = 5
  CB_MOUSEPRESSED = 6
  CB_MOUSERELEASED = 7
  CB_MOUSEMOVED = 8

cdef public int MAX_SPRITES = 2000

events = {}
cdef float *cspr = <float*>malloc(MAX_SPRITES*16*sizeof(float))

def set_title(title):
  core_set_title(title)

def register(num, func):
  events[num] = func

def draw_sprites(spriteid, n, sprites):
  cdef int i
  assert n < MAX_SPRITES
  for i in range(n*16):
    cspr[i] = sprites[i]
  core_draw_sprites(spriteid, n, cspr)

def load_sprite(fn):
  cdef int w
  cdef int h
  id = core_load_sprite(fn, &w, &h)
  return id, w, h

def set_resolution(w, h):
  core_set_resolution(w, h)

def set_scaling(factor, mode=0):
  core_set_scaling(factor, mode)

def set_bg_color(r,g,b):
  core_set_bg_color(r,g,b)

def quit():
  core_quit()

def sound_load(fn):
  return core_sound_load(fn)

def sound_play(n):
  core_sound_play(n)

def music_load(fn):
  return core_music_load(fn)

def music_play(n):
  core_music_play(n)

cdef public void callback(int num):
  events[num]()

cdef public void callback_i(int num, int arg0):
  events[num](arg0)

cdef public void callback_f(int num, float arg0):
  events[num](arg0)

cdef public void callback_ii(int num, int arg0, int arg1):
  events[num](arg0, arg1)

cdef public void callback_iii(int num, int arg0, int arg1, int arg2):
  events[num](arg0, arg1, arg2)
