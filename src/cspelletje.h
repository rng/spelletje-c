#ifndef __PYX_HAVE__cspelletje
#define __PYX_HAVE__cspelletje


/* "cspelletje.pyx":18
 *   void core_quit()
 * 
 * cdef public enum CallbackIds:             # <<<<<<<<<<<<<<
 *   CB_DRAW = 1
 *   CB_UPDATE = 2
 */
enum CallbackIds {
  CB_DRAW = 1,
  CB_UPDATE = 2,
  CB_RESIZE = 3,
  CB_KEYDOWN = 4,
  CB_KEYUP = 5,
  CB_MOUSEPRESSED = 6,
  CB_MOUSERELEASED = 7,
  CB_MOUSEMOVED = 8
};

#ifndef __PYX_HAVE_API__cspelletje

#ifndef __PYX_EXTERN_C
  #ifdef __cplusplus
    #define __PYX_EXTERN_C extern "C"
  #else
    #define __PYX_EXTERN_C extern
  #endif
#endif

__PYX_EXTERN_C DL_IMPORT(void) callback(int);
__PYX_EXTERN_C DL_IMPORT(void) callback_i(int, int);
__PYX_EXTERN_C DL_IMPORT(void) callback_f(int, float);
__PYX_EXTERN_C DL_IMPORT(void) callback_ii(int, int, int);
__PYX_EXTERN_C DL_IMPORT(void) callback_iii(int, int, int, int);

__PYX_EXTERN_C DL_IMPORT(int) MAX_SPRITES;

#endif /* !__PYX_HAVE_API__cspelletje */

#if PY_MAJOR_VERSION < 3
PyMODINIT_FUNC initcspelletje(void);
#else
PyMODINIT_FUNC PyInit_cspelletje(void);
#endif

#endif /* !__PYX_HAVE__cspelletje */
