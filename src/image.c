#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

uint8_t* image_load_png(const char* filename, int* w, int* h) {
  int n;
  return stbi_load(filename, w, h, &n, 4);
}
