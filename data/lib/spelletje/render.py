import cspelletje
from .font import Font
from .sprite import Sprite, SpriteAtlas


class DrawList(object):
  def __init__(self):
    self.sprites = []
    self.n = 0


class Render(object):
  def __init__(self):
    self._sprites = {}
    self.size = (0,0)
    self.scalefactor = 1
    self._blank = self.loadSprite("data/sprite/blank.png")

  def resize(self, w, h, scalefactor):
    self.size = (w, h)
    self.scalefactor = scalefactor

  def width(self):
    return self.size[0]/self.scalefactor

  def height(self):
    return self.size[1]/self.scalefactor

  def halfWidth(self):
    return self.size[0]/self.scalefactor/2

  def halfHeight(self):
    return self.size[1]/self.scalefactor/2

  def setBackgroundColor(self, r, g, b):
    cspelletje.set_bg_color(r,g,b)

  def loadSprite(self, fn, fw=0, fh=0):
    if fn not in self._sprites:
      self._sprites[fn] = Sprite(self, fn, fw, fh)
    return self._sprites[fn]
  
  def loadSpriteAtlas(self, fn):
    if fn not in self._sprites:
      self._sprites[fn] = SpriteAtlas(self, fn)
    return self._sprites[fn]
  
  def loadFont(self, fn):
    if fn not in self._sprites:
      self._sprites[fn] = Font(self, fn)
    return self._sprites[fn]

  #def layer(self, fn, num):
  #  return self.
  
  def drawSprite(self, spriteid, layer,x,y,cx,cy,w,h,tx0,ty0,tx1,ty1,angle,col):
    if (layer, spriteid) not in self._draws:
      d = self._draws[(layer, spriteid)] = DrawList()
    else:
      d = self._draws[(layer, spriteid)]
    camx,camy = self._state.camera
    r,g,b,a = col
    d.sprites.extend([x-camx,y-camy,cx,cy,w,h,tx0,ty0,tx1,ty1,angle,r,g,b,a,0])
    d.n += 1

  def drawRect(self, x1, y1, x2, y2, layer=100, col=None):
    col = col if col else (1,1,1,1)
    w,h = x2-x1, y2-y1
    camx,camy = self._state.camera
    x,y = x1+camx,y1+camy
    self.drawSprite(self._blank.id, layer, x,y, 0,0, w, h, 0, 0, 1, 1, 0, col)

  def startFrame(self, state):
    self._state = state
    self._draws = {}

  def endFrame(self):
    for layersprite in sorted(self._draws):
      dl = self._draws[layersprite]
      _, spriteid = layersprite
      cspelletje.draw_sprites(spriteid, dl.n, dl.sprites)
