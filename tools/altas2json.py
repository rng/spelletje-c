#!/usr/bin/env python
import json
import sys
from collections import defaultdict

def main():
  fn = sys.argv[1]
  lines = open(fn).readlines()
  spritename = None
  spriteprops = {}
  sprites = defaultdict(list)
  for line in lines:
    line = line.rstrip()
    if line.startswith(" "):
      prop, value = line.strip().split(":")
      value = value.strip()
      if value in ["true", "false"]:
        value = value == "true"
      elif "," in value:
        value = map(int,value.split(","))
      else:
        value = int(value)
      spriteprops[prop] =value
    else:
      if len(spriteprops):
        sprites[spritename].append(spriteprops)
      spritename = line
      spriteprops = {}

  json.dump(sprites, open(fn.replace("atlas","json"), "w"))


main()
