import cspelletje
import json


class Font(object):
  def __init__(self, render, fn):
    self.render = render
    self.fn = fn
    self.id, self.w, self.h = cspelletje.load_sprite(fn)
    self.chars = json.load(open(fn.replace(".png",".json")))

  def _charw(self, c):
    xadv = self.chars[ord(c)].get("xadv")
    if xadv is not None:
      return xadv
    if c == ' ':
      return self.chars[ord('j')]["w"]
    return self.chars[ord(c)]["w"]
  
  def _charh(self, c):
    return self.chars[ord(c)]["h"]

  def drawChar(self, x, y, char, layer, col, scale):
    c = ord(char)
    tw, th = self.w, self.h
    char = self.chars[c]
    w = char["w"]
    h = char["h"]
    y += char.get("yoff",0)
    if w == 0:
      return
    cx = char["x"]
    cy = char["y"]
    tx0 = cx/float(tw)
    ty0 = cy/float(th)
    tx1 = (cx+w)/float(tw)
    ty1 = (cy+h)/float(th)
    self.render.drawSprite(self.id, layer, x, y, 0, 0, w*scale, h*scale, 
                           tx0,ty0,tx1,ty1, 0, col)

  def size(self, s, scale=1, charpad=0):
    w = h = 0
    for c in s:
      w += (self._charw(c)+charpad)*scale
      h = max(h, self._charh(c)*scale)
    w -= scale
    return w, h

  def draw(self, x, y, s, centre=False, layer=0, col=None, scale=1, charpad=0):
    if centre:
      w, _ = self.size(s, scale, charpad=charpad)
      x -= w/2
    col = col if col else (1,1,1,1)
    for i, c in enumerate(s):
      self.drawChar(x, y, c, layer, col, scale)
      x += (self._charw(c)+charpad)*scale

