from . import entity

class Scene(object):
  def __init__(self):
    self.camera = (0,0)
    self.entities = entity.Entities()

  def update(self, dt):
    self.entities.update(dt)
  
  def draw(self):
    self.entities.draw()

  def enterScene(self, args):
    pass

  def leaveScene(self):
    pass
  
  def key_press(self, sym, mods):
    pass

  def key_release(self, sym, mods):
    pass
  
  def mouse_move(self, x, y):
    pass

  def mouse_press(self, x, y, button):
    pass

  def mouse_release(self, x, y, button):
    pass

  def mouse_scroll(self, x, y, sx, sy):
    pass
