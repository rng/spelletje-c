# vim:set ts=2 sw=2 sts=2 et:
from .collidable import Collidable, Collision
import math


class BaseLayer(object):
  def __init__(self, map, name, w, h, tw, th, layer):
    self.map = map
    self.name = name
    self.tw, self.th = tw, th
    self.layer = layer
    self.setSize(w, h)
  
  def setSize(self, w, h):
    self.width, self.height = w, h

  def draw(self, camera):
    pass


class ObjectLayer(BaseLayer):
  def __init__(self, map, name, w, h, tw, th, layer):
    BaseLayer.__init__(self, map, name, w, h, tw, th, layer)
    self.objects = []

  def addObject(self, o):
    self.objects.append(o)


class MapLayer(BaseLayer):
  
  def setSize(self, w, h):
    self.width, self.height = w, h
    self.tiles = [[0 for i in range(w)] for j in range(h)]

  def setTileset(self, ts):
    self.tileset = ts

  def setTile(self, x, y, v):
    self.tiles[y][x] = v
    
  def getTile(self, x, y):
    if (x >= self.width) or (y >= self.height):
      return 0
    return self.tiles[y][x]


class TileLayer(MapLayer):
  def __init__(self, map, name, w, h, tw, th, layer):
    MapLayer.__init__(self, map, name, w, h, tw, th, layer)
    self.opacity = 1

  def setOpacity(self, a):
    self.opacity = a

  def draw(self, camera):
    ts = self.tw
    engine = self.map.engine
    w, h = engine.render.size
    vw, vh = w/ts, h/ts
    # origin
    ox, oy = camera
    oxt, oyt = int(ox/float(ts)), int(oy/float(ts))
    pxofsx = abs(ox)%ts
    pxofsy = abs(oy)%ts
    if ox < 0: pxofsx = - pxofsx
    if oy < 0: pxofsy = - pxofsy
    col = [1,1,1,self.opacity]
    for i in range(vw+1):
      x = int((i*ts)-pxofsx+ox)
      tx = oxt + i
      for j in range(vh+1):
        y = int((j*ts)-pxofsy+oy)
        ty = oyt + j
        if (tx >= 0) and (ty >= 0) and (tx < self.width) and (ty < self.height):
          tid = self.tiles[ty][tx]-1
          if tid >= 0:
            self.tileset.draw(x, y, tid, col=col)


class CollisionLayer(MapLayer):

  def check(self, x, y, vx, vy, w, h):
    # calculate number of tiles we will pass through given vx,vy
    steps = math.ceil(max(abs(vx), abs(vy))/float(self.tw))
    if steps == 0:
      steps = 1
    # split velocity into a per step velocity
    sx = vx / steps
    sy = vy / steps
    res = Collision(x, y)
    for i in range(int(steps)):
      self._step(res, x, y, sx, sy, w, h, i)
      x = res.px
      y = res.py
      if res.collx:
        sx = 0
      if res.colly:
        sy = 0
      if not (sx or sy):
        break
    return res

  def _step(self, res, x, y, vx, vy, w, h, step):
    res.px += vx
    res.py += vy
    tilesize = self.tw
    ftilesize = float(tilesize)
    # check x axis
    if vx:
      pxoffsetx = w if vx > 0 else 0
      tilex = int((res.px + pxoffsetx) / ftilesize)
      if (tilex >= 0) and (tilex < self.width):
        starty = int(max(math.floor(y / ftilesize), 0))
        endy = int(min(math.ceil((y+h) / ftilesize), self.height))
        for tiley in range(starty, endy):
          if self.solid(tilex, tiley):
            res.collx = True
            tileoffsetx = tilesize if vx < 0 else 0
            res.tx = tilex
            res.px = tilex * tilesize - pxoffsetx + tileoffsetx
            break
    # check y axis
    if vy:
      pxoffsety = h if vy > 0 else 0
      tiley = int((res.py + pxoffsety) / ftilesize)
      if (tiley >= 0) and (tiley < self.height):
        startx = int(max(math.floor(res.px / ftilesize), 0))
        endx = int(min(math.ceil((res.px+w) / ftilesize), self.width))
        for tilex in range(startx, endx):
          if self.solid(tilex, tiley):
            res.colly = True
            tileoffsety = tilesize if vy < 0 else 0
            res.ty = tiley
            res.py = tiley * tilesize - pxoffsety + tileoffsety
            break

  def solid(self, x, y):
    return self.tiles[y][x] > 0


class Map(Collidable):
  def __init__(self, engine):
    self.engine = engine
    self.width = self.height = 0
    self.clear()

  def clear(self):
    self.layers = []
    self.named = {}

  def size(self):
    return self.width, self.height

  def pixelSize(self):
    tw,th = self.layers[0].tw, self.layers[0].th
    return self.width*tw, self.height*th

  def setSize(self, w, h):
    self.width, self.height = w, h

  def addTileLayer(self, name, num, tilesize):
    l = TileLayer(self, name, self.width, self.height, tilesize, tilesize, num)
    self.layers.append(l)
    self.named[name] = l
    return l

  def addLayer(self, layer):
    self.layers.append(layer)
    self.named[layer.name] = layer

  def draw(self, camera=(0,0)):
    for i, l in enumerate(self.layers):
      l.draw(camera)

  def tilePos(self, px, py):
    ts = self.layers[0].tw
    return px/ts, py/ts

  def collide(self, other, dt):
    return self.named["collision"].check(other.x, other.y,
                other.vx*dt, other.vy*dt, other.w, other.h)

  def solid(self, x, y):
    return self.named["collision"].solid(x, y)

