#include <stdio.h>
#include "backend_sfml.h"

int main(int argc, char *argv[]) {
  Core core;
  core_create(&core);
  return sfml_main(&core);
}
