#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "backend_sfml.h"
#include "backend.h"
#include "ogl.h"
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/Audio.hpp>

typedef struct {
  sf::Window *window;
  sf::Clock *clock;
  Core *core;
  Backend *backend;
  // textures
  uint32_t textures[64];
  int texturecount;
  // sounds
  sf::SoundBuffer soundbufs[32];
  int soundcount;
  sf::Sound sounds[64];
  // musics
  int musiccount;
  sf::Music musics[4];
  char *wintitle;
  bool quit;
  // screen stuff
  int scalefactor;
} BackendPriv;


void sfml_init(BackendPriv *priv) {
  int w = 480;
  int h = 320;

  sf::VideoMode mode(w, h);
  priv->window = new sf::Window(mode, "Spelletje", sf::Style::Close);
  //priv->window->setVerticalSyncEnabled(true);
  priv->window->setFramerateLimit(60);
  priv->window->setKeyRepeatEnabled(false);
  priv->clock = new sf::Clock();
  
  priv->wintitle = (char*)malloc(10);
  strcpy(priv->wintitle, "Spelletje");

  ogl_init(w, h, priv->scalefactor);

  priv->core->init(priv->backend);
}


void process_events(BackendPriv* priv) {
  // handle events
  sf::Event event;
  while (priv->window->pollEvent(event)) {
    if (event.type == sf::Event::Closed) {
      priv->quit = true;
    }
    else if (event.type == sf::Event::Resized) {
      ogl_init(event.size.width, event.size.height, priv->scalefactor);
      priv->core->resize(event.size.width, event.size.height, priv->scalefactor);
    } else if (event.type == sf::Event::KeyPressed) {
      priv->core->keydown(event.key.code, 0);
    } else if (event.type == sf::Event::KeyReleased) {
      priv->core->keyup(event.key.code, 0);
    } else if (event.type == sf::Event::MouseButtonPressed) {
      priv->core->mousepressed(event.mouseButton.x, event.mouseButton.y,
                               event.mouseButton.button);
    } else if (event.type == sf::Event::MouseButtonReleased) {
      priv->core->mousereleased(event.mouseButton.x, event.mouseButton.y,
                                event.mouseButton.button);
    } else if (event.type == sf::Event::MouseMoved) {
      priv->core->mousemoved(event.mouseMove.x, event.mouseMove.y);
    }
  }
}

uint64_t get_ticks(BackendPriv *priv) {
  sf::Time t = priv->clock->getElapsedTime();
  return t.asMicroseconds();
}


void sfml_run(BackendPriv *priv) {
  uint64_t lastFPS = 0, lastDrawTime = 0;
  float avgdt = 0;

  while (!priv->quit) {
    // process events
    process_events(priv);
    // update
    uint64_t sinceLastDraw = get_ticks(priv) - lastDrawTime;
    lastDrawTime = get_ticks(priv);
    double dt = (double)(sinceLastDraw)/1000000.;
    priv->core->update(dt);
    // draw
    ogl_start_frame();
    priv->core->draw();
    ogl_finish_frame();
    // update framerate display
    float weight = 0.9;
    avgdt = avgdt * weight + dt * (1-weight);
    if ((lastDrawTime - lastFPS) > 500000) {
      lastFPS = lastDrawTime;
      char title[64];
      int fps = (int)(1./avgdt);
      sprintf(title, "%s [%dfps]", priv->wintitle, fps);
      priv->window->setTitle(title);
    }
    // swap
    priv->window->display();
  }

}


void sfml_shutdown(BackendPriv *priv) {
  priv->core->shutdown();
  if (priv->wintitle) {
    free(priv->wintitle);
  }
  // cleanup and shutdown
  delete priv->clock;
  priv->window->close();
  delete priv->window;
}


void sfml_set_title(Backend* backend, const char *title) {
  BackendPriv *priv = (BackendPriv*)backend->priv;
  priv->window->setTitle(title);
  free(priv->wintitle);
  priv->wintitle = (char*)malloc(strlen(title)+1);
  strcpy(priv->wintitle, title);
}


void sfml_set_fps(Backend* backend, int fps) {
  BackendPriv *priv = (BackendPriv*)backend->priv;
  priv->window->setFramerateLimit(fps);
}


int sfml_create_image(Backend* backend, int w, int h, uint8_t *data) {
  BackendPriv *priv = (BackendPriv*)backend->priv;
  uint32_t tex = ogl_load_texture(w, h, data);
  priv->textures[priv->texturecount] = tex;
  priv->texturecount++;
  return tex;
}


void sfml_set_bg_color(Backend* backend, float r, float g, float b) {
  BackendPriv *priv = (BackendPriv*)backend->priv;
  ogl_set_bg_color(r,g,b);
}


void sfml_set_resolution(Backend* backend, int w, int h) {
  BackendPriv *priv = (BackendPriv*)backend->priv;
  sf::VideoMode mode(w, h);
  priv->window->create(mode, priv->wintitle, sf::Style::Close);
}

void sfml_set_scaling(Backend* backend, int factor, int mode) {
  BackendPriv *priv = (BackendPriv*)backend->priv;
  sf::Vector2u size = priv->window->getSize();
  priv->scalefactor = factor;
  sfml_set_resolution(backend, size.x*factor, size.y*factor);
}

void sfml_draw_quads(Backend *backend, int sprite, int n, float* pos, float* tex, uint8_t* col) {
  ogl_draw_quads(sprite, n, pos, tex, col);
}


int sfml_sound_load(Backend *backend, const char *fn) {
  BackendPriv *priv = (BackendPriv*)backend->priv;
  int n = priv->soundcount++;
  priv->soundbufs[n].loadFromFile(fn);
  return n;
}


void sfml_sound_play(Backend *backend, int n) {
  BackendPriv *priv = (BackendPriv*)backend->priv;
  for (int i=0;i<64;i++) {
    if (priv->sounds[i].getStatus() != sf::Sound::Playing) {
      priv->sounds[i].setBuffer(priv->soundbufs[n]);
      priv->sounds[i].play();
      break;
    }
  }
}


int sfml_music_load(Backend *backend, const char *fn) {
  BackendPriv *priv = (BackendPriv*)backend->priv;
  int n = priv->musiccount++;
  priv->musics[n].openFromFile(fn);
  return n;
}


void sfml_music_play(Backend *backend, int n) {
  BackendPriv *priv = (BackendPriv*)backend->priv;
  priv->musics[n].play();
}


void sfml_quit(Backend *backend) {
  BackendPriv *priv = (BackendPriv*)backend->priv;
  priv->quit = true;
}


int sfml_main(Core *core) {
  BackendPriv priv;
  Backend backend;
  priv.core = core;
  priv.backend = &backend;
  priv.texturecount = 0;
  priv.soundcount = 0;
  priv.musiccount = 0;
  priv.wintitle = 0;
  priv.scalefactor = 1;
  priv.quit = false;

  // init backend struct
  backend.priv = (void*)&priv;
  backend.set_title = sfml_set_title;
  backend.set_fps = sfml_set_fps;
  backend.set_resolution = sfml_set_resolution;
  backend.set_scaling = sfml_set_scaling;
  backend.set_bg_color = sfml_set_bg_color;
  backend.create_image = sfml_create_image;
  backend.draw_quads = sfml_draw_quads;
  backend.sound_load = sfml_sound_load;
  backend.sound_play = sfml_sound_play;
  backend.music_load = sfml_music_load;
  backend.music_play = sfml_music_play;
  backend.quit = sfml_quit;

  sfml_init(&priv);
  sfml_set_fps(&backend, 60);
  sfml_run(&priv);
  sfml_shutdown(&priv);
  return 0;
}
